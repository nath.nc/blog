Oui mais des contraintes qu'on se donne à soit même.

Ne plus manger de viande, faire sur sport tout les jours, ne plus aller sur facebook, ne plus prendre l'avion, faire un plein maxium par mois...

la seule limite est l'imagination :) 

C'est une bonne façon de découvrir des choses. Quand on est dans le concrêt on se rend compte de choses qu'on aurait jamais pu voir de loin. C'est amusant, ça fait réfléchir et ça ouvre la voie à d'autres idées..

J'ai testé :
- ne plus manger de viande, ni de produits laitiers, pendant 3 mois : il y aurait pas mal à dire sur ça
- ne plus aller sur facebook au travail (oui ça fait du bien)

En cours, le gros challenge, ne plus prendre l'avion. Déjà rien que le fait de me l'être dit me fait réfléchir à pleins de façon de faire pour remplir cet objectif.

Tchao !