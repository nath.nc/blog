Pfiou, aujourd'hui j'ai voulu aller au taff à vélo !
Ca faisait quelques semaines que j'avais ça en tête, depuis que j'ai découvert que mon nouveau coloc a un vélo.

Bon, ça ne s'est pas passé comme dans mon imagination, comme quoi, il fallait bien essayer :)

Ca commençait pourtant bien, je me réveille une heure avant mon réveil habituel, je me dis que je vais en profiter pour accomplir cet exploit. Pti déj préparation toussa toussa. Et puis je vais voir le véol en question, que je n'avais jamais essayer. Pneues dégonflé, c'est pas grave la pompe est dessus, on regonfle ça en 2 2. J'essaye la feraille et je me rends compte que c'est bien un véol de descente et que c'est pas le plus adapté. J'y vais ou j'y vais pas ? Après quelques hésitations, j'acte: j'y vais.

Je fais mon sac, prépare de quoi me doucher au travail, un peu d'eau, je mets la chaine que mon autre coloc m'a prêté, oups, c'est un peu lourd... he oui c'est une chaîne pour son bateau :s 
Avec ça le poids de mon sac ne laisse pas indiférent mon petit dos..

Bon aller, c'est parti, je sens que c'est pas le top du top, mais ça fait tellement longtemps que j'ai pas fait de vélo que je ne suis pas capable de me projeter. Donc j'y vais j'essaye.

Niveau itinéraire j'habite à 8km, à vol d'oiseau, de mon travail. Ca paraît pas beaucoup sauf que c'est à Nouméa, les pistes cyclables sont assez limitées et le relief assez marqué. J'ai prévu de passer par la voie du Néo bus qui passe pas très loin de chez moi. C'est officiellement interdit, mais officieusement toléré. Pour la rejoindre, je dois commencer par franchir une colline. Temps total estimé : 45 minutes.

Je sors de chez moi sur la bécane, un virage, un petit chemin pieton en terre, une petite montée en terre, traverser la route, entamer la colline, souffler souffler souffler... là je me rends compte que ça va être très très chaud ! Je n'arrive pas à finir la première montée, je termine à pied. J'arrive en haut et continue, je me dis que la suite sera plus plate que le plus dure est passé, et puis le plaisir de la descente qui suit me fait relativiser, la fraicheur du vent dans la face, mmmmh. J'arrive comme ça sur la voie du bus, là où c'est sensé allé tout seul, mais en fait non. Même sur quasiment du plat je galère. Là je me dis que le vélo n'est définitivement pas adapté, oui, la faute du matériel. 

Je réfléchis deux secondes, je me pause deux minutes, et prend la décision d'arrêter le carnage. C'est un échec, mais cette sortie me fait du bien quand même, autant que quand je fais un footing matinal. 
Je dois quand même rentrer et me retaper la colline, cette fois j'y vais plus doucement, vitesse 1:1, j'avais peut être trop forcé à l'aller.

Retour au bercail, douche, sac a dos, scooter. Finalement je ne serai pas en retard malgrès ce détour expérimental. 
Et en plus je suis en pleine forme :) 

Conclusion : se préparer, tester le matériel et ses capacités avant.
Deuxième conclusion : ne pas se préparer et ne pas tester le matériel, pour pouvoir vivre des échecs heureux

Bonne journée.
