Arrêtons de penser que le développement informatique est simple.

Et l'une des choses qui le rend compliqué, est qu'on pense que c'est simple.
Ainsi on ne fait pas d'effort pour le simplifier et on se retrouve dans des situations compliquées.

"C'est juste ça à faire, ça prend deux minutes" ... 3 heures plus tard ... "bon, en fait, y avait ça aussi, et puis ça impliquait ceci, et j'ai du faire une reprise sur cela"

Adiós 