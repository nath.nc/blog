Arrêtez, arrêtons de nous moquer.

Se moquer c'est se fermer des portes, c'est s'empêcher de pouvoir réaliser des choses qui sortent de l'ordinaire, voire, nos rêves.

Quand on est proche de se lancer sur quelque chose d'original, on peut avoir peur du regard des autres et de la moquerie. Je pense que cette peur est d'autant plus grande qu'on a tendance à porter un jugement moqueur sur les autres.

Quand je dis "original" je parle de quelque chose qui sort de l'ordinaire pour soi-même.

Exemple: Bobby s'est souvent moqué des véliplanchistes débutants qui avaient du mal à lever leur voile, qui adoptaient une posture étrange (du fait de la force du vent), et qui se cassaient la gueule. Lol, oui c'est drôle mdr. Aujourd'hui qu'on lui propose d'essayer la planche à voile, il dénigre ce sport et trouve toutes les excuses pour ne pas monter sur la planche. Il refuse, il est mieux sur la plage. Dommage, il aurait découvert un sport stylé, peut être même une nouvelle passion.

Se moquer c'est se fermer.

Pour nous et pour les autres, gratifions-nous, encourageons-nous, soyons libre.

Kissous